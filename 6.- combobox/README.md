# Combobox

## Mostrar en un label la opción seleccionada

![Captura de pantalla del proyecto](https://gitlab.com/gabokuck/PyQt5/raw/master/6.-%20combobox/Captura.PNG)

Borrar y añadir items desde codigo

``` python
import sys
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5 import uic

class Dialogo(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        uic.loadUi("combobox.ui", self)
        self.boton.clicked.connect(self.getItem)

        #Agregar nuevo item
        self.lenguajes.addItem("C++")

        #Eliminar un item
        self.lenguajes.removeItem(0)

    def getItem(self):
        item = self.lenguajes.currentText()
        self.labelLenguajes.setText("Has seleccionado: " + item)

app = QApplication(sys.argv)
dialogo = Dialogo()
dialogo.show()
app.exec_()
```