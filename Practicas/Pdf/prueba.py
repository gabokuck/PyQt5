from reportlab.pdfgen import canvas #clase canvas para crear el documento
from reportlab.lib.pagesizes import letter, A4 #clase para hacer eldocumento en tamaño A4
from reportlab.lib.colors import white, red, green, blue, gray, black #Colores
from os import startfile #para abrir eldocumento

nombre_archivo = "mi_pdf.pdf" #nombre del pdf a crear
pdf = canvas.Canvas(nombre_archivo,pagesize=letter) #crear el documento pdf y se establece en tamaño A4

cx = 100
cy = 710
ancho = 300
alto = 80

pdf.setFillColor(red)
pdf.rect(cx, cy, ancho, alto, fill=1)
pdf.setFillColor(green)
pdf.rect(cx + ancho, cy, ancho, alto, fill=1)

pdf.drawString(100,100,"Hello World")




pdf.showPage() # detiene la escritura en el archivo
pdf.save() # Guarda el archivo en el directorio actual
startfile(nombre_archivo) #abre automaticamente el archivo pdf creado.