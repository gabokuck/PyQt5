import sys 
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5 import uic

class Cuadro_Dialogo(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        uic.loadUi("saludar.ui", self)
        self.juntarNombre
        self.boton.clicked.connect(self.juntarNombre)


    def juntarNombre(self):
        appa = self.lineApPa.text()
        apma = self.lineApMa.text()
        nombre = self.lineNombre.text()
        self.labelSaludar.setText("Saludar: " + appa.capitalize() + " " + apma.capitalize() + " " + nombre.capitalize())

app = QApplication(sys.argv)
dialogo = Cuadro_Dialogo()
dialogo.show()
app.exec_()