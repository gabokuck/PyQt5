import sys
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5 import uic

class Dialogo(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        uic.loadUi("credencial.ui", self)
        self.boton.clicked.connect(self.generarCredencial)

    def generarCredencial(self):
        appa = self.lineApPa.text()
        apma = self.lineApMa.text()
        nombre = self.lineNombre.text()
        self.labelAlumno.setText("Nombre del alumno(a): " + appa.capitalize() + " " + apma.capitalize() + " " + nombre.capitalize() )


app = QApplication(sys.argv)
dialogo = Dialogo()
dialogo.show()
app.exec_()