##import os
##import pdfkit
##import wkhtmltopdf
from weasyprint import HTML
##from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader("templates"))
template = env.get_template("index.html")

usuario = {
    'name' : 'Gabriel',
    'course' : 'Python',
    'score' : 9.5,
    'date' : 'Fecha actual'
}

html1 = template.render(usuario)


HTML(html1).write_pdf('/tmp/weasyprint-website.pdf')