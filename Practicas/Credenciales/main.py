import sys
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5 import uic
#
from reportlab.pdfgen import canvas #clase canvas para crear el documento
from reportlab.lib.pagesizes import letter, A4 #clase para hacer eldocumento en tamaño A4
from reportlab.lib.colors import white, red, green, blue, gray, black #Colores
from os import startfile #para abrir eldocumento
from reportlab.platypus import Image

class Dialogo(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        uic.loadUi("Credencial.ui", self)
        self.boton.clicked.connect(self.generarCredencial)

    def generarCredencial(self):
        appa = self.lineApPa.text()
        apma = self.lineApMa.text()
        nombre = self.lineNombre.text()
        nombre_completo = appa.capitalize() + " " + apma.capitalize() + " " + nombre.capitalize()
        self.labelNombre.setText(nombre_completo)

        #Emepieza generar credencial pdf
        nombre_archivo = "mi_pdf.pdf" #nombre del pdf a crear
        pdf = canvas.Canvas(nombre_archivo,pagesize=letter) #crear el documento pdf y se establece en tamaño A4

        #Rectangulo con bordes redondos
        #pdf.roundRect(40, 600, 100, 150, 5, stroke=1, fill=0)
        #Texto
        #pdf.drawString(50,750,nombre_completo)
        # Cordenadas para imprimir
        #Eje y
        pdf.drawString(0,0,"0, 0")
        pdf.drawString(0,10,"0, 10")
        pdf.drawString(0,20,"0, 20")
        pdf.drawString(0,30,"0, 30")
        pdf.drawString(0,40,"0, 40")
        pdf.drawString(0,50,"0, 50")
        pdf.drawString(0,60,"0, 60")
        pdf.drawString(0,70,"0, 70")
        pdf.drawString(0,390,"0, 390")
        pdf.drawString(0,780,"0, 780")
        #Eje x
        pdf.drawString(560,0,"560, 0")
        pdf.drawString(560,390,"560, 390")
        pdf.drawString(560,780,"560, 780")
        #Imagen
        #imagen = 'logo.png'
        #pdf.drawImage('logo.png', 50, 730, width=50, height=60, mask='auto')
        


        pdf.showPage() # detiene la escritura en el archivo
        pdf.save() # Guarda el archivo en el directorio actual
        startfile(nombre_archivo) #abre automaticamente el archivo pdf creado.
        #termina generar credencial pdf

app = QApplication(sys.argv)
dialogo = Dialogo()
dialogo.show()
app.exec_()