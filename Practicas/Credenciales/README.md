# Generar credencial

![Captura de pantalla](https://gitlab.com/gabokuck/PyQt5/raw/master/Practicas/Credenciales/Captura.PNG)

# Para generar .exe 

* Instalar pyinstaller
* Usar comando pyinstaller main.py
    * Para generar un solo archivo pyinstaller main.py -F
    * Para que no aparesa la consola pyintaller main.py -w